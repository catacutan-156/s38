// DOM Selectors
// const firstName = document.querySelector("#firstName");
// const lastName = document.querySelector("#lastName");

const firstName = document.getElementById("firstName");
const lastName = document.getElementById("lastName");

const inputFields = document.getElementsByClassName("form-control");

const heading = document.getElementsByTagName("h3");

console.log(heading);

console.log(firstName);
console.log(lastName);

// console.log(typeof firstName);
// console.log(typeof lastName);

console.log(firstName.value);

console.log(inputFields);